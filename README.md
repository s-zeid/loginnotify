LoginNotify CraftBukkit Plugin
==============================
Notifies the owner of a CraftBukkit server whenever a player logs in.

Copyright (c) 2013 Scott Zeid.  Released under the X11 License.  
<http://code.s.zeid.me/loginnotify>

This plugin displays a notification on the server's machine whenever
someone logs on to the server.  By default, it supports libnotify on
Linux by using the `notify-send` command, but other notification
systems can be used as long as they support using commands to display
notifications.

The command's working directory is set to the plugin's data folder
(i.e. `plugins/LoginNotify`).

Configuration
-------------

See `config.yml` (either in the plugin's data folder after running
it once, or in the source tree under `src/main/resources/`) for
configuration instructions.  To reload the configuration at runtime,
run `/loginnotify reload` either in-game or on the server console.

Permissions
-----------

 * `loginnotify.reload`
   
    Reloads the server configuration.  
    **Default:** Operators only

Compiling
---------

To manually build the plugin, run `mvn` from the root of the source tree.
You will need a working Internet connection in order for Maven to download
the appropriate dependencies.  The compiled JAR file will be written to
`target/LoginNotify-<version>.jar`.
