/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBukkit LoginNotify Plugin
   Notifies the owner of a CraftBukkit server whenever a player logs in.
   
   Copyright (C) 2013 Scott Zeid
   http://code.s.zeid.me/loginnotify
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.loginnotify;

import java.io.IOException;
import java.lang.ProcessBuilder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import org.stringtemplate.v4.*;

public final class LoginNotify extends JavaPlugin {
 private ST makeST(String tpl) {
  return new ST(tpl, '{', '}');
 }
 
 @Override
 public void onEnable() {
  this.saveDefaultConfig();
  getServer().getPluginManager().registerEvents(new Listener() {
   @EventHandler
   public void onPlayerLogin(PlayerLoginEvent e) throws IOException {
    FileConfiguration config = LoginNotify.this.getConfig();
    
    String name = e.getPlayer().getPlayerListName();
    
    List<String> blacklist = config.getStringList("players.blacklist");
    List<String> whitelist = config.getStringList("players.whitelist");
    if (whitelist.size() > 0) {
     if (!whitelist.contains(name)) return;
    } else {
     if (blacklist.contains(name)) return;
    }
    
    String address = e.getAddress().getHostAddress();
    String destHost = e.getHostname();
    String datetime = new SimpleDateFormat(config.getString("datetime-format"))
                      .format(new Date());
    
    String[][] vars = {
     {"name", name},
     {"address", address},
     {"datetime", datetime},
     {"destHost", destHost}
    };
    
    ST titleTpl = makeST(config.getString("notification.title"));
    ST bodyTpl  = makeST(config.getString("notification.body"));
    ST[] messageTpls = {titleTpl, bodyTpl};
    for (ST tpl : messageTpls) {
     for (String[] var : vars)
      tpl.add(var[0], var[1]);
    }
    
    String title = titleTpl.render();
    String body  = bodyTpl.render();
    
    List<String> commandTpl = config.getStringList("command");
    List<String> command = new ArrayList<String>();
    for (String arg : commandTpl) {
     ST argumentTpl = makeST(arg);
     for (String[] var : vars)
      argumentTpl.add(var[0], var[1]);
     argumentTpl.add("title", title);
     argumentTpl.add("body", body);
     arg = argumentTpl.render();
     command.add(arg);
    }
    
    ProcessBuilder p = new ProcessBuilder(command);
    p.directory(LoginNotify.this.getDataFolder());
    p.start();
   }
  }, this);
 }
 
 @Override
 public void onDisable() {
  HandlerList.unregisterAll(this);
 }
 
 @Override
 public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
  if (cmd.getName().equalsIgnoreCase("loginnotify")) {
   if (args.length == 1 && args[0].equals("reload")) {
    this.reloadConfig();
    sender.sendMessage(ChatColor.GREEN
                        + "LoginNotify configuration reloaded."
                        + ChatColor.RESET);
    return true;
   }
  }
  return false;
 }
}
